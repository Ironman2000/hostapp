from django.shortcuts import render
from .models import Sentence

# Create your views here.


def home_page(request):
    sentences = Sentence.objects.all()
    return render(request, 'index.html', {'sentences': sentences})
